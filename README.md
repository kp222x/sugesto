# SUGESTO #

### What is Sugesto for? ###

Design and Develop the website where users can share their ideas with others and receive feedback and
suggestions online. The ideas can be from the field of Business, Computers &amp; IT, Sports, etc. or general
thoughts depending on the user’s what they want to share or get reviewed upon. The publisher can write
their ideas under any category as deemed necessary and other users can write comments on those ideas,
give ratings, feedbacks and suggestions. The focus is to provide an online platform for people working in
group and will be similar to brainstorming method, but in the form of an online web-system, making the
process less cumbersome and faster. Posts with most popularity from each category should be displayed
on feature page for everyone else to view.

### Developer ###

[Karan Parikh](http://www.karanparikh.tk/)