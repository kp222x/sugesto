<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>Sugesto</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link href="https://fonts.googleapis.com/css?family=Pacifico|Roboto" rel="stylesheet">
<link rel="stylesheet" href="css/animate.css">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script>
 	new WOW().init();
</script>

</head>
<body>
<%@taglib prefix="C" uri="http://java.sun.com/jstl/core_rt" %>
<C:set var="fail" value="${sessionScope.fail}"></C:set>

<div class="container">
<div class="row text-center">
	<div class="col-12">
    <h1 class="title wow fadeInDown">Sugesto</h1>
    </div>
</div>
<div class="row">
	<div class="col-12">
	
    <div class="form">
   <C:if test="${fail eq true}">
		<div class="loginfail wow fadeIn">
			<a  href="signup.jsp"><p style="color: white;">Try again, Wrong UserName or Password!!</p></a>    
    	</div>
   </C:if>
        <form action="index.jsp" method="post" id="login-form">
          <div class="forceColor wow flipInY"></div>
          <div class="topbar wow flipInY">
            <div class="spanColor wow flipInY"></div>
            <input type="text" name="username" class="input" id="userName" placeholder="User Name" required="required"/>
            <input type="password" name="password" class="input" id="password" placeholder="Password" required="required"/>
            
            <input type="hidden" name="flag" value="login"/>
          </div>
          <button type="submit" name="login" class="submit wow flipInY" id="submit" >Login</button>
        </form>
        
        </div>
    </div>
</div>
<div class="row wow fadeIn">
	<div class="col-12 signup">
		<a class="nodecor" href="signup.jsp"><p>haven't got an account? <span class="signupLink">Sign up</span></p></a>    
    </div>
</div>
</div>
</body>
</html>
