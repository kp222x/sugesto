<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Sugesto Wall</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Pacifico|Roboto" rel="stylesheet">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/wall.css">
<link rel="stylesheet" href="css/radio.css">
<link rel="stylesheet" href="css/section.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<style>
body{
width: calc(100vw - 34px);
overflow-y: scroll;
}
</style>

<script>
 	new WOW().init();
</script>

</head>

<body>
<%@taglib prefix="C" uri="http://java.sun.com/jstl/core_rt" %>
<C:set var="userid" value="${sessionScope.userid}"></C:set>
<C:set var="headername" value="${sessionScope.headername}"></C:set>
<C:set var="username" value="${sessionScope.username}"></C:set>
<C:set var="starcount" value="${sessionScope.starcount}"></C:set>
<C:set var="suggestioncount" value="${sessionScope.suggetioncount}"></C:set>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand title" href="index.jsp">Sugesto</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav ">
        <li class="active"><a href="index.jsp"><span class="glyphicon glyphicon-home"></span> Home</a></li>
        <li class=""><a href="myideas.jsp"><span class="glyphicon glyphicon-list-alt"></span> Your Ideas</a></li>        
        <li class=""><a href="trending.jsp"><span class="glyphicon glyphicon-stats"></span> Trending</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-phone-alt"></span> Contact</a></li>
        <li><a href="login.jsp?flag=logout"><span class="glyphicon glyphicon-log-out"></span> logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid shadow">
</div>

<div class="container" style="margin-top:30px">
  <div class="row">
  	<div class="col-lg-3 col-md-3 col-sm-3">
    	<div class="col-lg-12 col-md-12 col-sm-12">
    	<div class=" wow fadeInLeft">
        	<article class="row">
              <figure class="thumbnail userDetails">
                <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg" />
                <figcaption class="leftuser"><span class="leftusername">${headername}</span> <span class="leftuserid">@${username}</span></figcaption>
                    <div class="totalPost">
                    	<div class="pull-left left">
                        	<span class="leftlable">Suggestions</span>                          
                        	<p class="score text-center">${suggestioncount}</p>
                    	</div>
                        <div class="pull-right right">
                        	<span class="leftlable">Stars</span>                          
                        	<p class="score text-center">${starcount}</p>
                        </div>
                    </div>
              </figure>
              
            </article>
         </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">
    	<div class="col-lg-12 col-md-12 col-sm-12">
         <form action="<%=request.getContextPath()%>/WallController"  name="post" method="post" id="post-form">
         <div class="panel-body panel-200">
          <textarea class="form-control expandable wow fadeInDown" type="text" name="userpost" id="myIn" placeholder="What are you thinking?" rows="3"></textarea>
          <input type="hidden" name="flag" value="post"/>
          <button type="button" id="bttnsubmit" data-toggle="modal" data-target="#myModal" class="btn btn-primary postBtn wow fadeInDown">Sugest</button>
				
				  <!-- Modal -->
				  <div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog">
				    
				      <!-- Modal content-->
				      <div class="modal-content">
				        <div class="modal-header bg-primary">
				          <h4 class="modal-title">Select category in which your idea falls in!</h4>
				        </div>
				        <div class="modal-body">
				          <div class="text-center">
							  <label class="radio inline"> 
							      <input type="radio" name="categories" value="1" checked>
							      <span> Technology </span> 
							   </label>
							  <label class="radio inline"> 
							      <input type="radio" name="categories" value="2">
							      <span> Buisness </span> 
							  </label>
							  <label class="radio inline"> 
							      <input type="radio" name="categories" value="3">
							      <span> Health </span> 
							  </label>
							</div>
				        </div>
				        <div class="modal-footer">
          				<button  type="submit" class="btn btn-primary postBtn wow fadeInDown">Sugest</button>
				        </div>
				      </div>
				      
				    </div>
				  </div>
         </form>
         <div class="space"></div>
<div id="postContent">
<C:set var="userid" value="${sessionScope.userid}"></C:set>
<C:set var="noPosts" value="${sessionScope.noPosts}"></C:set>
<C:choose>
<C:when test="${noPosts eq true}">
<div class="mainContent main wow fadeIn">
          	<div class="panel-group" data-wow-delay="0.2s">
              <div class="panel panel-default">
                <div class="panel-body">
                <h4>No Ideas to display in your selected Interest(s) !</h4>
                <p>Tell us What you're thinking above or try changing your interest.</p>
                </div>
              </div>
            </div>
          </div>
</C:when>
<C:otherwise>
<C:forEach items="${sessionScope.sugestoposts}" var="obj">
          <div class="mainContent main wow fadeIn">
          	<div class="panel-group" data-wow-delay="0.2s">
              <div class="panel panel-default">
                <div class="panel-heading"><span class="name">${obj.name}</span> &nbsp;<span class="username">@${obj.username}</span>&nbsp;<span class="username" style="float: right;">${obj.cv.name}</span></div>
                <div class="panel-body">${obj.post}</div>
                <div class="feedback">
                
               <C:set var="postid" value="${obj.post_id}"></C:set>
               <C:set var="userStared" value="false"></C:set>
               <C:forEach items="${sessionScope.stars}" var="starObj">
               <C:set var="starUserid" value="${starObj.user_id}"></C:set>
               <C:set var="starPostid" value="${starObj.post_id}"></C:set>
               <C:set var="stared" value="${starObj.stared}"></C:set>
               
               <C:choose>
               	<C:when test="${userid eq starUserid && postid eq starPostid && stared eq true}">
					<C:set var="userStared" value="true"></C:set>
				</C:when>
               </C:choose>
               </C:forEach> 
               
               <C:choose>
               <C:when test="${userStared eq true}">
               <a style="cursor: pointer;"><span id="star-icon" class="glyphicon glyphicon-glyphicon glyphicon-star" onclick="starPost('${sessionScope.userid}', '${obj.post_id}', 'star')"></span></a><span>${obj.stars}</span>&nbsp;
               </C:when>
               <C:otherwise>
               <a style="cursor: pointer;"><span id="star-icon" class="glyphicon glyphicon-glyphicon glyphicon-star-empty" onclick="starPost('${sessionScope.userid}', '${obj.post_id}', 'star')"></span></a><span>${obj.stars}</span>&nbsp;
               </C:otherwise>
               </C:choose>
               
               
               
                <a href="comments.jsp?flag=comment&userid=${obj.v.id}&postid=${obj.post_id}"><span class="glyphicon glyphicon-comment"></span>&nbsp;${obj.comments}</a>
                </div>
              </div>
            </div>
          </div>
</C:forEach>
</C:otherwise>
</C:choose>
</div>

         </div>
        </div>
        
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3">
    	<div class="col-lg-12 col-md-12 col-sm-12">
        	
            <div class="panel panel-default wow fadeInRight ">
                <!-- Default panel contents -->
                <div class="panel-heading">Your Interest</div>
            
                <!-- List group -->
                <ul class="list-group">
                <C:forEach items="${sessionScope.categories}" var="obj">
                <C:set var="category" value="false"></C:set>
                <C:forEach items="${sessionScope.userCategories}" var="ucObj">
                <C:choose>
                <C:when test="${obj.category_id eq ucObj.cv.category_id}">
                <C:set var="category" value="true"></C:set>
                
                </C:when>
                </C:choose>
                
              	</C:forEach>
                    <li class="list-group-item">
                    <div class="material-switch pull-left">
                    <C:choose>
               		 <C:when test="${category eq true}">
                            <input id="someSwitchOptionDefault${obj.category_id}" name="someSwitchOption${obj.category_id}" type="checkbox" onchange="changeCategory('${obj.category_id}','false')" checked/>
                           </C:when>
                           <C:otherwise>
                           <input id="someSwitchOptionDefault${obj.category_id}" name="someSwitchOption${obj.category_id}" type="checkbox" onchange="changeCategory('${obj.category_id}', 'true')"/>
                           </C:otherwise>
                     </C:choose>
                            <label for="someSwitchOptionDefault${obj.category_id}" class="label-primary"></label>
                     
                     </div>
                     <span>&nbsp; ${obj.name} </span>
      
              </li>
   				</C:forEach>
              
                </ul>
            </div>  
            
            <div class="panel panel-default wow fadeInRight">
                <div class="panel-body">
                	<span class="about">&copy; Sugesto 2017</span>
                </div>
  				<div class="panel-heading">
                	<a class="adv" href="#"><span class="glyphicon glyphicon-new-window"></span> Advertise with Sugesto</a>
                </div>
            </div>
    
        </div>
                      
    </div>
    
  </div>
 
</div>
</body>
</html>
<script>
function starPost(userid, postid, flag){
	
	$.ajax({
		url:"<%=request.getContextPath()%>/WallController",
		data: {postid: postid, userid: userid, flag: flag},
		method: "GET", 
		success: function(result) {
			document.getElementById("postContent").innerHTML = result;
		}
		
	});
	
}

function changeCategory(category_id, is_on){

	var id = "someSwitchOptionDefault"+category_id;
	var is_on;
	
	if(document.getElementById(id).checked == false) {
		console.log('you turned '+category_id+' off!');
		is_on = false;
	}
	else{
		console.log('you turned '+category_id+' on!');
		is_on = true;
	}
	
	 $.ajax({
		url:"<%=request.getContextPath()%>/WallController",
		data: {category_id: category_id, is_on: is_on, flag: "cat"},
		method: "POST", 
		success: function(result) {
			document.getElementById("postContent").innerHTML = result;
		}
		
	}); 
	
}

$(document).ready(function() {
    $('#bttnsubmit').attr('disabled','disabled');
    $('#myIn').keyup(function() {
       if($(this).val() != '') {
          $('#bttnsubmit').removeAttr('disabled');
       }
       else {
       $('#bttnsubmit').attr('disabled','disabled');
       }
    });
});
</script>


