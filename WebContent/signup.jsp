<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>Sugesto</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
<link rel="stylesheet" href="css/signup.css">
<link href="https://fonts.googleapis.com/css?family=Pacifico|Roboto" rel="stylesheet">
<link rel="stylesheet" href="css/animate.css">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script>
 	new WOW().init();
</script>

</head>
<body>
<div class="container signupPage">
<div class="row text-center">
	<div class="col-12">
    <h1 class="join wow fadeInDown"><span class="jointext">Create your </span>Sugesto<span class="jointext"> account</span></h1>
    </div>
</div>
<div class="row">
	<div class="col-12">
        <div class="formSignup">
        
         <form action="<%=request.getContextPath()%>/UserController"  name="registration" method="post" id="signup-form">
          <div class="forceColor wow flipInY"></div>
          <div class="topbar wow flipInY">
            <div class="spanColor wow flipInY"></div>
            <input type="text" name="firstname" class="input" id="firstname" placeholder="First Name" required="required"/>
            <input type="text" name="lastname" class="input" id="lastname" placeholder="Last Name" required="required"/>
            <input type="text" name="username" class="input" id="username" placeholder="User Name" required="required"/>
            <input type="password" name="password" class="input" id="password"  placeholder="Password" required="required"/>
                        <span id='message' style="font-size: 16px; font-weight:100"></span>
            
            <input type="password" name="password" class="input" id="confirm_password" placeholder="Confirm Password" required="required"/>
            
            <input type="hidden" name="flag" value="Registration"/>
          </div>
          <button type="submit" name="signup" class="submit wow flipInY" id="submit" >Sign Up</button>
         </form>
          
        </div>
    </div>
</div>
<div class="row wow fadeIn">
	<div class="col-12 logIn">
		<a class="nodecor" href="login.jsp"><p>Already with Sugesto? <span class="logInLink">Login</span></p></a>    
    </div>
</div>
</div>
</body>
</html>
<script>
$('#password, #confirm_password').on('keyup', function () {
    if ($('#password').val() == $('#confirm_password').val()) {
        $('#message').html('').css('color', 'green');
    } else 
        $('#message').html('Password Not Matching').css('color', 'white');
});
</script>