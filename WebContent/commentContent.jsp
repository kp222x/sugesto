<%@taglib prefix="C" uri="http://java.sun.com/jstl/core_rt" %>
<C:forEach items="${sessionScope.postComments}" var="obj">
        <section class="comment-list">
          <article class="row">
              <div class="panel panel-default arrow left wow fadeIn">
                <div class="panel-body">
                  <header class="text-left">
                    <div class="comment-user "><i class="glyphicon glyphicon-user"></i>&nbsp;<span class="username">@${obj.mv.username}</span></div>
                    <time class="comment-date " datetime="16-12-2014 01:05"><i class="glyphicon glyphicon-time"></i>&nbsp;<span class="username">${obj.date_added}</span></time>
                  </header>
                  <div class="comment-post">
                    <p>
						${obj.comment}
                    </p>
                  </div>
                </div>
              </div>
          </article>
        </section>
</C:forEach>