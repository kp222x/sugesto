<%@taglib prefix="C" uri="http://java.sun.com/jstl/core_rt" %>
<div id="postContent">
<C:set var="userid" value="${sessionScope.userid}"></C:set>
<C:set var="noPosts" value="${sessionScope.noPosts}"></C:set>
<C:choose>
<C:when test="${noPosts eq true}">
<div class="mainContent main wow fadeIn">
          	<div class="panel-group" data-wow-delay="0.2s">
              <div class="panel panel-default">
                <div class="panel-body">
                <h4>Sorry You Haven't Posted Any Ideas Yet!</h4>
                <p>Take a chance and tell the world, what you're thinking!</p>
                </div>
              </div>
            </div>
          </div>
</C:when>
<C:otherwise>
<C:forEach items="${sessionScope.myideas}" var="obj">
          <div class="mainContent main">
          	<div class="panel-group" data-wow-delay="0.2s">
              <div class="panel panel-default">
                <div class="panel-heading"><span class="name">${obj.name}</span> &nbsp;<span class="username">@${obj.username}</span>&nbsp;<span class="username" style="float: right;">${obj.cv.name}</span></div>
                <div class="panel-body">${obj.post}</div>
                <div class="feedback">
                
               <C:set var="postid" value="${obj.post_id}"></C:set>
               <C:set var="userStared" value="false"></C:set>
               <C:forEach items="${sessionScope.stars}" var="starObj">
               <C:set var="starUserid" value="${starObj.user_id}"></C:set>
               <C:set var="starPostid" value="${starObj.post_id}"></C:set>
               <C:set var="stared" value="${starObj.stared}"></C:set>
               
               <C:choose>
               	<C:when test="${userid eq starUserid && postid eq starPostid && stared eq true}">
					<C:set var="userStared" value="true"></C:set>
				</C:when>
               </C:choose>
               </C:forEach> 
               
               <C:choose>
               <C:when test="${userStared eq true}">
               <a style="cursor: pointer;"><span id="star-icon" class="glyphicon glyphicon-glyphicon glyphicon-star" onclick="starPost('${sessionScope.userid}', '${obj.post_id}', 'starIdeas')"></span><span>${obj.stars}</span></a>&nbsp;
               </C:when>
               <C:otherwise>
               <a style="cursor: pointer;"><span id="star-icon" class="glyphicon glyphicon-glyphicon glyphicon-star-empty" onclick="starPost('${sessionScope.userid}', '${obj.post_id}', 'starIdeas')"></span><span>${obj.stars}</span></a>&nbsp;
               </C:otherwise>
               </C:choose>
               
               
               
                <a href="comments.jsp?flag=comment&userid=${obj.v.id}&postid=${obj.post_id}"><span class="glyphicon glyphicon-comment"></span>&nbsp;${obj.comments}</a>
                </div>
              </div>
            </div>
          </div>
</C:forEach>
</C:otherwise>
</C:choose>
</div>