package authentication;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.hibernate.Session;

import vo.CategoryVo;
import vo.CommentVo;
import vo.UserCategoryVo;
import vo.UserMstVo;
import vo.UserVo;
import vo.WallVo;
import dao.CommentDao;
import dao.UserCategoryDao;
import dao.UserDao;
import dao.UserMstDao;
import dao.WallDao;

/**
 * Servlet Filter implementation class Filter
 */
@WebFilter("/*")
public class Filter implements javax.servlet.Filter {

    /**
     * Default constructor. 
     */
    public Filter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		HttpSession session = ((HttpServletRequest) request).getSession();
		RequestDispatcher requestDispatcher;
		String uri = ((HttpServletRequest)request).getRequestURI();
		
			String flag = null;
			flag = request.getParameter("flag");
			String userid=null;
		    try
		    {
		    	userid = session.getAttribute("userid").toString();
		    }
		    catch(Exception e)
		    {
		    	userid = null;
		    }
		    
		if(flag!=null && flag.equals("login"))
		{
			user_login(request,response,chain);	
		}
		if(flag!=null && flag.equals("logout"))
		{
			user_logout(request,response,chain);	
		}
		
		if(uri.contains("login")||uri.contains("doc")||uri.contains("fileupload")||uri.contains("signup")||uri.contains("/css") || uri.contains("/js")||uri.contains("Controller")||uri.contains("UploadServlet")|| uri.contains("/img")|| uri.contains("/font"))
		{
			if(uri.contains("login")||uri.contains("signup"))
			{
				System.out.println("login found");
				try
			    {
			    	userid = session.getAttribute("userid").toString();
			    }
			    catch(Exception e)
			    {
			    	userid = null;
			    }
				
				if(userid == null)
				{
					chain.doFilter(request, response);
				}
				else
				{
					requestDispatcher = request.getRequestDispatcher("index.jsp");  
					requestDispatcher.forward(request,response);
				}
			}
			else
			{
				chain.doFilter(request, response);
			}
		}
		else if(uri.contains("index.jsp") || uri.contains("comments.jsp") || uri.contains("postContent.jsp") || uri.contains("categoryContent.jsp") || uri.contains("commentPost.jsp") || uri.contains("commentContent.jsp") || uri.contains("trending.jsp") || uri.contains("trendPost.jsp") || uri.contains("myideas.jsp") || uri.contains("ideaPost.jsp")){
			try
		    {
		    	userid = session.getAttribute("userid").toString();
		    }
		    catch(Exception e)
		    {
		    	userid = null;
		    }
			
			if(uri.contains("index.jsp") && userid != null)
			{
				session.setAttribute("index", "active");
				
				getPosts(request,response,chain);
				getPostStars(request,response, chain);
				userCategory(request,response);
				chain.doFilter(request, response);
			}
			else if(uri.contains("comments.jsp") && userid != null){
				commentPost(request,response);
				getComments(request,response);
				chain.doFilter(request, response);
			}
			else if(uri.contains("postContent.jsp") && userid != null){
				getPosts(request,response,chain);
				getPostStars(request,response, chain);
				chain.doFilter(request, response);
			}
			else if(uri.contains("categoryContent.jsp") && userid != null){
				getPosts(request,response,chain);
				getPostStars(request,response, chain);
				chain.doFilter(request, response);
			}
			else if(uri.contains("commentPost.jsp") && userid != null){
				commentPost(request,response);
				getPostStars(request,response, chain);
				chain.doFilter(request, response);
			}
			else if(uri.contains("commentContent.jsp") && userid != null){
				getComments(request,response);
				chain.doFilter(request, response);
			}
			else if(uri.contains("trending.jsp") && userid != null){
				getTrend(request,response);
				getPostStars(request,response, chain);
				chain.doFilter(request, response);
			}
			else if(uri.contains("trendPost.jsp") && userid != null){
				getTrend(request,response);
				getPostStars(request,response, chain);
				chain.doFilter(request, response);
			}
			else if(uri.contains("myideas.jsp") && userid != null){
				getIdeas(request,response);
				getPostStars(request,response, chain);
				chain.doFilter(request, response);
			}
			else if(uri.contains("ideaPost.jsp") && userid != null){
				getIdeas(request,response);
				getPostStars(request,response, chain);
				chain.doFilter(request, response);
			}
			else
			{
				requestDispatcher = request.getRequestDispatcher("login.jsp");  
				requestDispatcher.forward(request,response);
			}
		}
		else
		{
			requestDispatcher = request.getRequestDispatcher("login.jsp");  
			requestDispatcher.forward(request,response);
		}
	}

	private void getIdeas(ServletRequest request, ServletResponse response) {
		// TODO Auto-generated method stub
		HttpSession session = ((HttpServletRequest) request).getSession();
		
		String userid;
		try{
			userid = session.getAttribute("userid").toString();
		}catch(Exception e){
			userid = "";
		}
		
		WallVo wv = new WallVo();
		UserDao ud = new UserDao();
		UserMstVo mv = new UserMstVo();
		
		mv.setId(Long.parseLong(userid));
		wv.setV(mv);
		
		List ls = ud.getIdeas(wv); 
		
		if(ls !=null && ls.size()>=1){
			session.setAttribute("myideas", ls);
			session.setAttribute("noPosts", false);
		}else{
			session.setAttribute("noPosts", true);
		}
	}

	

	private void userCategory(ServletRequest request, ServletResponse response) {
		// TODO Auto-generated method stub
		HttpSession session = ((HttpServletRequest) request).getSession();

		String user_id = session.getAttribute("userid").toString();
		
		UserMstVo mv = new UserMstVo();
		UserCategoryVo uv = new UserCategoryVo();
		UserCategoryDao ucd = new UserCategoryDao();
		
		mv.setId(Long.parseLong(user_id));
		uv.setV(mv);
		
		List ls = ucd.getOnCategories(uv);
		
		if(ls !=null && ls.size()>=1){
			session.setAttribute("userCategories", ls);
		}
		
		List ls1 = ucd.getCategories();
		if(ls1 !=null && ls1.size()>=1){
			session.setAttribute("categories", ls1);
		}
	
	}

	

	private void getPosts(ServletRequest request, ServletResponse response,
			FilterChain chain) {
		// TODO Auto-generated method stub

		HttpSession session = ((HttpServletRequest) request).getSession();
		
		String userid = session.getAttribute("userid").toString();
		
		WallDao wd = new WallDao();
		List ls = null;
		
		ls= wd.posts(Long.parseLong(userid));
		if(ls !=null && ls.size()>=1){
			session.setAttribute("sugestoposts", ls);
			session.setAttribute("noPosts", false);
		}
		else{
			session.setAttribute("noPosts", true);
		}
	}

	private void user_login(ServletRequest request, ServletResponse response,
			FilterChain chain) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = ((HttpServletRequest) request).getSession();
		List ls = null;
		RequestDispatcher requestDispatcher;
		
		UserMstDao d2 = new UserMstDao();
		UserMstVo v2 =  new UserMstVo();
		UserDao d1 = new UserDao();
		UserVo v1 = new UserVo();
		WallVo wv = new WallVo();
		WallDao wd = new WallDao();
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		v2.setUsername(username);
		v2.setPassword(password);
		
		ls = d2.login(v2);
		
		if(ls !=null && ls.size()>=1){
			
			session.removeAttribute("fail");
			Iterator itr = ls.iterator();
			
			UserMstVo usermst = (UserMstVo) itr.next();
			long id = usermst.getId();
			username = usermst.getUsername();
			session.setAttribute("username", username);
			
			v2.setId(id);
			wv.setV(v2);
			List wallList = wd.getUsersTotalStars(wv);
			List sugCount = wd.getUserSugCount(wv);
			
			long starCount; 
			long sugessionCount;
			try{
				starCount = (Long)wallList.get(0);
			}catch(Exception e){
				starCount = 0;
			}
			try{ 
				sugessionCount = (Long)sugCount.get(0);
			}catch(Exception e){
				sugessionCount = 0;
			}
				session.removeAttribute("starcount");
				session.setAttribute("starcount", starCount);			
				session.removeAttribute("suggetioncount");
				session.setAttribute("suggetioncount", sugessionCount);
			
			UserVo user = new UserVo();
			user.setId(id);
			
			List ls2 = d1.search(user);
			Iterator itr2 = ls2.iterator();
			UserVo user1 = (UserVo) itr2.next();
			boolean access = user1.isAccess();
			if(!access){
				requestDispatcher = request.getRequestDispatcher("blocked.jsp");  
				requestDispatcher.forward(request,response);
			}
			else{
				String firstname = user1.getFirstname();
				String lastname = user1.getLastname();
				session.setAttribute("userid",id);
				session.setAttribute("headername",firstname +" "+ lastname);
			}	
		} else{
			session.setAttribute("fail", true);
		}
	}
	
	private void user_logout(ServletRequest request, ServletResponse response,
			FilterChain chain) {
		// TODO Auto-generated method stub
		HttpSession session = ((HttpServletRequest) request).getSession();
		session.invalidate();
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
