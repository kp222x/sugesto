package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.http.HTTPBinding;

import dao.CommentDao;
import vo.CommentVo;
import vo.PostFeedbackVo;
import vo.UserMstVo;
import vo.WallVo;

/**
 * Servlet implementation class CommentController
 */
@WebServlet("/CommentController")
public class CommentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String flag = request.getParameter("flag");
		
		if(flag.equals("comment")){
			
			insertComment(request,response);
			response.sendRedirect("commentContent.jsp");
		}
	}

	private void insertComment(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		
		String comment = request.getParameter("comment");
		String postid = request.getParameter("postid");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String date_added = sdf.format(new Date());
		
		session.setAttribute("postid", postid);
		String userid;
		try{
			userid =  session.getAttribute("userid").toString();
		}
		catch(Exception e){
			userid = null;
		}
		
		UserMstVo mv = new UserMstVo();
		WallVo wv = new WallVo();
		CommentVo cv = new CommentVo();
		CommentDao cd = new CommentDao();
		
		cv.setComment(comment);
		cv.setDate_added(date_added);
		mv.setId(Long.parseLong(userid));
		wv.setPost_id(Long.parseLong(postid));
		cv.setMv(mv);
		cv.setWv(wv);
		
		cd.insertComment(cv);
		
		List ls = cd.getCount(wv);
		if(ls !=null && ls.size()>=1){
			Iterator itr = ls.iterator();
			
			WallVo wv1 = (WallVo) itr.next();
			long count = wv1.getComments();
			count = count + 1;
		
			wv.setComments(count);
			cd.updateCommentCount(wv);
		}
	}
}
