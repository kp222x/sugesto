package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import dao.UserMstDao;
import vo.UserMstVo;
import vo.UserVo;

/**
 * Servlet implementation class UserController
 */
@WebServlet("/UserController")
	public class UserController extends HttpServlet {
		private static final long serialVersionUID = 1L;
	       
	    /**
	     * @see HttpServlet#HttpServlet()
	     */
	    public UserController() {
	        super();
	        // TODO Auto-generated constructor stub
	    }
	
		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
		}
	
		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			
				String flag = request.getParameter("flag");
				
				if(flag.equals("Registration")){
					
					insert(request,response);
					login(request,response);
					response.sendRedirect("index.jsp");
				}
			}
	
		private void login(HttpServletRequest request,
				HttpServletResponse response) {
			// TODO Auto-generated method stub
			String password = request.getParameter("password");
			String username = request.getParameter("username");

			HttpSession session = request.getSession();
			
			List ls = null;
			UserVo v1 = new UserVo();
			UserDao d1 = new UserDao();
			UserMstVo v2 = new UserMstVo();
			UserMstDao d2 = new UserMstDao();
			
			v2.setUsername(username);
			v2.setPassword(password);
			
			ls = d2.login(v2);
			
			if(ls != null && ls.size()>=1){
				
				Iterator itr = ls.iterator();
				UserMstVo userMst = (UserMstVo) itr.next();
				long id = userMst.getId();
				String username1 = userMst.getUsername();
				session.setAttribute("userid",id);
				session.setAttribute("username", username1);
				
				long starCount = 0; 
				long sugessionCount = 0;
				session.removeAttribute("starcount");
				session.setAttribute("starcount", starCount);			
				session.removeAttribute("suggetioncount");
				session.setAttribute("suggetioncount", sugessionCount);
					
					
				UserVo user = new UserVo();
				user.setId(id);
					
				List ls2 = d1.search(user);
				Iterator itr2 = ls2.iterator();
				UserVo usernm = (UserVo) itr2.next();
				
				String firstname = usernm.getFirstname();
				String lastname = usernm.getLastname();
				session.setAttribute("headername",firstname +" "+ lastname);
				}	
		}


		private void insert(HttpServletRequest request, HttpServletResponse response) {
			// TODO Auto-generated method stub
			
			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String date_added = sdf.format(new Date());
			
			UserVo v1 = new UserVo();
			UserDao d1 = new UserDao();
			UserMstVo v2 = new UserMstVo();
			UserMstDao d2 = new UserMstDao();
		
			v2.setUsername(username);
			v2.setPassword(password);
			
			v1.setFirstname(firstname);
			v1.setLastname(lastname);
			v1.setAccess(true);
			v1.setDate_added(date_added);
			v1.setV(v2);

			d2.insertUser(v2);
			d1.insertUser(v1);
		}
	}
