package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.taglibs.standard.lang.jpath.adapter.Convert;
import org.hibernate.Session;

import dao.UserCategoryDao;
import dao.WallDao;
import vo.CategoryVo;
import vo.PostFeedbackVo;
import vo.UserCategoryVo;
import vo.UserMstVo;
import vo.WallVo;

/**
 * Servlet implementation class WallController
 */
@WebServlet("/WallController")
public class WallController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WallController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String flag = request.getParameter("flag");
		if(flag.equals("star")){
			insertFeedback(request,response);
			response.sendRedirect("postContent.jsp");
		}
		else if(flag.equals("commentStar")){
			insertFeedback(request,response);
			response.sendRedirect("commentPost.jsp");
		}
		else if(flag.equals("starTrend")){
			insertFeedback(request,response);
			response.sendRedirect("trendPost.jsp");
		}
		else if(flag.equals("starIdeas")){
			insertFeedback(request,response);
			response.sendRedirect("ideaPost.jsp");
		}
	}

	private void insertFeedback(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		String userid = request.getParameter("userid");
		String postid = request.getParameter("postid");
		String postUserid = request.getParameter("postUserid");
	
		HttpSession session = request.getSession();
		session.setAttribute("postUserid", postUserid);
		session.setAttribute("postid", postid);

		WallDao wd = new WallDao();
		PostFeedbackVo fv = new PostFeedbackVo();
		
		fv.setStared(true);
		fv.setUser_id(Long.parseLong(userid));
		fv.setPost_id(Long.parseLong(postid));
		
		List ls = wd.findFeedback(fv);
		
		if(ls !=null && ls.size()>=1){
			Iterator itr = ls.iterator();
			
			PostFeedbackVo fv1 = (PostFeedbackVo) itr.next();
			fv1.setUser_id(Long.parseLong(userid));
			fv1.setPost_id(Long.parseLong(postid));
			if(fv1.isStared()){
				wd.updateFeedbackFlase(fv1);
				decreaseStars(request,response);
			}else{
				wd.updateFeedbackTrue(fv1);
				increaseStars(request,response);
			}
		}
		else{
		wd.insertFeedback(fv);
		increaseStars(request,response);
		}
	}
	
	private void decreaseStars(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		String postid = request.getParameter("postid");
		long stars = 0;
		
		WallVo wv = new WallVo();
		WallDao wd = new WallDao();
		wv.setPost_id(Long.parseLong(postid));
		
		List ls = wd.getStars(wv);
		if(ls !=null && ls.size()>=1){
			Iterator itr = ls.iterator();
			
			WallVo wv1 = (WallVo) itr.next();
			stars = wv1.getStars();
			stars = stars - 1 ;
		}
		
		wv.setStars(stars);
		wd.updatePost(wv);
	}

	private void increaseStars(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		String postid = request.getParameter("postid");
		long stars = 0;
		
		WallVo wv = new WallVo();
		WallDao wd = new WallDao();
		wv.setPost_id(Long.parseLong(postid));
		
		List ls = wd.getStars(wv);
		if(ls !=null && ls.size()>=1){
			Iterator itr = ls.iterator();
			
			WallVo wv1 = (WallVo) itr.next();
			stars = wv1.getStars();
			stars = stars + 1 ;
		}
		
		wv.setStars(stars);
		wd.updatePost(wv);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String flag = request.getParameter("flag");
		
		if(flag.equals("post")){
			
			insertPost(request,response);
			response.sendRedirect("index.jsp");
		}
		else if(flag.equals("cat")){
			
			updateUserCategory(request,response);
			response.sendRedirect("categoryContent.jsp");
		}
	}

	private void updateUserCategory(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		String is_on = request.getParameter("is_on");
		String category_id = request.getParameter("category_id");
		String user_id = session.getAttribute("userid").toString();
		
		UserCategoryVo ucv = new UserCategoryVo();
		CategoryVo cv = new CategoryVo();
		UserMstVo umv = new UserMstVo();
		WallDao wd = new WallDao();
		
		cv.setCategory_id(Long.parseLong(category_id));
		umv.setId(Long.parseLong(user_id));
		
		ucv.setCv(cv);
		ucv.setV(umv);
		ucv.setIs_on(Boolean.parseBoolean(is_on));
		
		List ls = wd.checkCategory(ucv);
		
		if(ls.size()==1) {
			wd.updateCategory(ucv);
		}
		else {
			wd.insertCategory(ucv);
		}
		
		
		
	}

	private void insertPost(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		String userpost = request.getParameter("userpost");
		String category = request.getParameter("categories");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String date_added = sdf.format(new Date());
		
		HttpSession session = request.getSession();
		String userid;
		String name;
		String username;
		try
	    {
	    	userid = session.getAttribute("userid").toString();
	    	name = session.getAttribute("headername").toString();
	    	username = session.getAttribute("username").toString();
	    }
	    catch(Exception e)
	    {
	    	userid = null;
	    	name = "";
	    	username = "";
	    }
		
		
		WallVo v1 = new WallVo();
		WallDao d1 = new WallDao();
		UserMstVo v2 = new UserMstVo();
		CategoryVo cv = new CategoryVo();
		
		v1.setPost(userpost);
		v1.setStars(0);
		v1.setComments(0);
		v1.setDate_added(date_added);
		v1.setName(name);
		v1.setUsername(username);
		v2.setId(Long.parseLong(userid));
		cv.setCategory_id(Integer.parseInt(category));
		
		v1.setV(v2);
		v1.setCv(cv);
		
		d1.insertPost(v1);
		
		List ls = d1.getLastPost();
		long lastPostid = 0;
		
		if(ls !=null && ls.size()>=1){
			Iterator itr = ls.iterator();
			
			WallVo wv1 = (WallVo) itr.next();
			lastPostid = wv1.getPost_id();
		}
		
		PostFeedbackVo fv = new PostFeedbackVo();
		
		fv.setStared(false);
		fv.setUser_id(Long.parseLong(userid));
		fv.setPost_id(lastPostid);
		d1.insertFeedback(fv);
	}

}
