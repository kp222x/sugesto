package vo;

import java.io.Serializable;;

public class WallVo implements Serializable{

	UserMstVo v = new UserMstVo();
	CategoryVo cv = new CategoryVo();
	private long post_id;
	private String post;
	private long stars;
	private long comments;
	private String date_added;
	private String name;
	private String username;
	
	public long getComments() {
		return comments;
	}
	public void setComments(long comments) {
		this.comments = comments;
	}
	public long getStars() {
		return stars;
	}
	public void setStars(long stars) {
		this.stars = stars;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public CategoryVo getCv() {
		return cv;
	}
	public void setCv(CategoryVo cv) {
		this.cv = cv;
	}
	public UserMstVo getV() {
		return v;
	}
	public void setV(UserMstVo v) {
		this.v = v;
	}
	public long getPost_id() {
		return post_id;
	}
	public void setPost_id(long post_id) {
		this.post_id = post_id;
	}
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	public String getDate_added() {
		return date_added;
	}
	public void setDate_added(String date_added) {
		this.date_added = date_added;
	}
}
