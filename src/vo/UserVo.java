package vo;

import java.io.Serializable;

public class UserVo implements Serializable {
	
	UserMstVo v = new UserMstVo();
	private long id;
	private String firstname;
	private String lastname;
	private String date_added;
	private boolean access;
	
	public UserMstVo getV() {
		return v;
	}
	public void setV(UserMstVo v) {
		this.v = v;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getDate_added() {
		return date_added;
	}
	public void setDate_added(String date_added) {
		this.date_added = date_added;
	}
	public boolean isAccess() {
		return access;
	}
	public void setAccess(boolean access) {
		this.access = access;
	}

}
