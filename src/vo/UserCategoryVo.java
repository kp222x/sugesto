package vo;

import java.io.Serializable;
public class UserCategoryVo implements Serializable {

	long cat_id;
	CategoryVo cv = new CategoryVo();
	UserMstVo v = new UserMstVo();
	boolean is_on;
	
	
	public boolean isIs_on() {
		return is_on;
	}
	public void setIs_on(boolean is_on) {
		this.is_on = is_on;
	}
	public CategoryVo getCv() {
		return cv;
	}
	public void setCv(CategoryVo cv) {
		this.cv = cv;
	}

	public long getCat_id() {
		return cat_id;
	}
	public void setCat_id(long cat_id) {
		this.cat_id = cat_id;
	}
	public UserMstVo getV() {
		return v;
	}
	public void setV(UserMstVo v) {
		this.v = v;
	}
	
	
}
