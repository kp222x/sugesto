package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import vo.UserCategoryVo;


public class UserCategoryDao {

	public List getOnCategories(UserCategoryVo uv) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Query q = session.createQuery("FROM UserCategoryVo WHERE user_id='"+uv.getV().getId()+"' AND is_on = true");
		List ls = q.list();
		
		return ls;
	}

	public List getCategories() {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Query q = session.createQuery("FROM CategoryVo");
		List ls = q.list();
		
		return ls;	
	}
	

}
