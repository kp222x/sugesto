package dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import vo.PostFeedbackVo;
import vo.UserCategoryVo;
import vo.WallVo;

import org.apache.log4j.TTCCLayout;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class WallDao {

	public void insertPost(WallVo v1) {
		// TODO Auto-generated method stub
		try
		{		
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction tr = session.beginTransaction();
			session.save(v1);
			tr.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public List posts(long l) {
		// TODO Auto-generated method stub
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		System.out.println("got id: "+l);
		Query q = session.createQuery("FROM WallVo as wall WHERE wall.cv IN (SELECT U.cv FROM UserCategoryVo as U WHERE U.v = "+l+" AND U.is_on = true ) ORDER BY wall.id DESC");
		List ls = q.list();
		return ls;
	}
	
	
	

	public void insertFeedback(PostFeedbackVo fv) {
		// TODO Auto-generated method stub
		
		try
		{		
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction tr = session.beginTransaction();
			session.save(fv);
			tr.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public List getStars(WallVo wv) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Query q = session.createQuery("FROM WallVo WHERE post_id='"+wv.getPost_id()+"'");
		List ls = q.list();
		
		return ls;
	}

	public void updatePost(WallVo wv) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tr = session.beginTransaction();
		String hql = "UPDATE WallVo SET stars='"+wv.getStars()+"' WHERE post_id='"+wv.getPost_id()+"'";
		Query q = session.createQuery(hql);
		q.executeUpdate();
		tr.commit();
	}

	public List stars() {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Query q = session.createQuery("FROM PostFeedbackVo");
		List ls = q.list();
		
		return ls;
	}

	public List getLastPost() {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Query q = session.createQuery("FROM WallVo ORDER BY id DESC LIMIT 1");
		List ls = q.list();
		
		return ls;
	}

	public List findFeedback(PostFeedbackVo fv) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Query q = session.createQuery("FROM PostFeedbackVo WHERE user_id='"+fv.getUser_id()+"' AND post_id='"+fv.getPost_id()+"'");
		List ls = q.list();
		
		return ls;
	}

	public void updateFeedbackFlase(PostFeedbackVo fv1) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tr = session.beginTransaction();
		String hql = "UPDATE PostFeedbackVo SET stared=FALSE WHERE user_id='"+fv1.getUser_id()+"' AND post_id='"+fv1.getPost_id()+"'";
		Query q = session.createQuery(hql);
		q.executeUpdate();
		tr.commit();
	}
	
	public void updateFeedbackTrue(PostFeedbackVo fv1) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tr = session.beginTransaction();
		String hql = "UPDATE PostFeedbackVo SET stared=TRUE WHERE user_id='"+fv1.getUser_id()+"' AND post_id='"+fv1.getPost_id()+"'";
		Query q = session.createQuery(hql);
		q.executeUpdate();
		tr.commit();
	}

	public List getCommentPost(WallVo wv) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Query q = session.createQuery("FROM WallVo WHERE post_id='"+wv.getPost_id()+"' AND login_id='"+wv.getV().getId()+"'");
		List ls = q.list();
		System.out.println("Size ls" + ls.size());
		return ls;
	}

	public List checkCategory(UserCategoryVo ucv) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Query q = session.createQuery("FROM UserCategoryVo WHERE user_id='"+ucv.getV().getId()+"' AND category_id='"+ucv.getCv().getCategory_id()+"'");
		List ls = q.list();
		
		return ls;
	}

	public void updateCategory(UserCategoryVo ucv) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tr = session.beginTransaction();
		String hql = "UPDATE UserCategoryVo SET is_on = "+ucv.isIs_on()+" WHERE user_id='"+ucv.getV().getId()+"' AND category_id='"+ucv.getCv().getCategory_id()+"'";
		Query q = session.createQuery(hql);
		q.executeUpdate();
		tr.commit();
	}

	public void insertCategory(UserCategoryVo ucv) {
		// TODO Auto-generated method stub
		try
		{		
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction tr = session.beginTransaction();
			session.save(ucv);
			tr.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public List getUsersTotalStars(WallVo wv) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		String sumCount = "SELECT sum(c.stars) as total FROM WallVo c WHERE login_id='"+wv.getV().getId()+"'";
		Query q = session.createQuery(sumCount);
		List ls = q.list();
		return ls;
	}

	public List getUserSugCount(WallVo wv) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		String sumCount = "SELECT count(c.post) as total FROM WallVo c WHERE login_id='"+wv.getV().getId()+"'";
		Query q = session.createQuery(sumCount);
		List ls = q.list();
		return ls;	
	}

	public List getTopTech() {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		String sumCount = "FROM WallVo c WHERE (c.cv,c.stars) IN ( SELECT c1.cv, max(c1.stars) FROM WallVo c1 GROUP BY cv)";
		Query q = session.createQuery(sumCount);
		List ls = q.list();
		System.out.println("trend size"+ ls.size());
		return ls;
	}

}
